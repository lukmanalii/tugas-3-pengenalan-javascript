//Soal 1
var sentence1 = "saya sangat senang hari ini"; 

var kata_pertama = sentence1.substr(0, 5);
var kata_kedua = sentence1.substr(12, 7);
var frasa1 = kata_pertama.concat(kata_kedua);

var sentence2 = "belajar javascript itu keren";

var kata_satu = sentence2.substr(0, 8);
var kata_dua = sentence2.substr(8, 10);
var upper = kata_dua.toUpperCase();
var frasa2 = kata_satu.concat(upper);

console.log(frasa1.concat(frasa2));

//Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var angka1 = Number("10");
var angka2 = Number("2");
var angka3 = Number("4");
var angka4 = Number("6");

console.log(angka1 + angka2 * angka3 + angka4);

//Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kata1 = kalimat.substring(0, 3); 
var kata2 = kalimat.substring(4, 14);
var kata3 = kalimat.substring(15, 18);
var kata4 = kalimat.substring(19, 24); 
var kata5 = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kata1); 
console.log('Kata Kedua: ' + kata2); 
console.log('Kata Ketiga: ' + kata3); 
console.log('Kata Keempat: ' + kata4); 
console.log('Kata Kelima: ' + kata5);